# bash ../tikzmake.sh model5
import sys
sys.path.append('../')
from pycore.tikzeng import *

# defined your arch
arch = [
    to_head( '..' ),
    to_cor(),
    to_begin(),
    
    to_Conv("conv1", 28, 64, offset="(0,0,0)", to="(0,0,0)", height=64, depth=64, width=2, caption="Conv1"),
    to_Conv("conv2", 28, 64, offset="(1,0,0)", to="(conv1-east)", height=64, depth=64, width=2, caption="Conv2"),
    to_Pool("pool1", offset="(1,0,0)", height=64, depth=64, width=2, to="(conv2-east)", caption="Pool1"),
    to_Pool2("batch1", offset="(1,0,0)", height=64, depth=64, width=2, to="(pool1-east)", caption="Batch1"),
    
    to_Conv("conv3", 28, 128, offset="(6,0,0)", to="(batch1-east)", height=64, depth=64, width=2, caption="Conv3"),
    to_Conv("conv4", 28, 128, offset="(1,0,0)", to="(conv3-east)", height=64, depth=64, width=2, caption="Conv4"),
    to_Pool("pool2", offset="(1,0,0)", height=64, depth=64, width=2, to="(conv4-east)", caption="Pool2"),
    to_Pool2("batch2", offset="(1,0,0)", height=64, depth=64, width=2, to="(pool2-east)", caption="Batch2"),
    
    to_Conv("conv5", 28, 256, offset="(6,0,0)", to="(batch2-east)", height=64, depth=64, width=2, caption="Conv5"),
    to_Pool("pool3", offset="(1,0,0)", height=64, depth=64, width=2, to="(conv5-east)", caption="Pool3"),
    
    to_Pool("flat1", offset="(6,0,0)", height=64, depth=64, width=2, to="(pool3-east)", caption="Flat1"),
    to_Pool2("batch3", offset="(1,0,0)", height=64, depth=64, width=2, to="(flat1-east)", caption="Batch3"),
    
    #to_Conv("dense1", 1, 512, offset="(5,0,0)", to="(batch3-east)", height=64, depth=64, width=2, caption="Dense1"),
    #to_Conv("dense2", 1, 10, offset="(1,0,0)", to="(dense1-east)", height=64, depth=64, width=2, caption="Dense2"),
    
    to_SoftMax("dense1", 512 ,"(6,0,0)", "(batch3-east)", depth=150, caption="Dense1"),
    to_SoftMax("dense2", 10 ,"(2,0,0)", "(dense1-east)", depth=25, caption="Dense2"),
    
    
    #to_Conv("conv2", 128, 64, offset="(1,0,0)", to="(pool1-east)", height=32, depth=32, width=2 ),
    #to_connection( "pool1", "conv2"),
    #to_Pool("pool2", offset="(0,0,0)", to="(conv2-east)", height=28, depth=28, width=1),
    #to_SoftMax("soft1", 10 ,"(3,0,0)", "(pool1-east)", caption="SOFT"  ),
    #to_connection("pool2", "soft1"),
    to_end()
    ]

def main():
    namefile = str(sys.argv[0]).split('.')[0]
    to_generate(arch, namefile + '.tex' )

if __name__ == '__main__':
    main()
