# bash ../tikzmake.sh model3
import sys
sys.path.append('../')
from pycore.tikzeng import *

# defined your arch
arch = [
    to_head( '..' ),
    to_cor(),
    to_begin(),
    
    #to_Conv("conv1", 28, 32, offset="(0,0,0)", to="(0,0,0)", height=64, depth=64, width=2, caption="Conv1"),
    #to_Pool("pool1", offset="(1,0,0)", height=64, depth=64, width=2, to="(conv1-east)", caption="Pool1"),
    
    #to_Conv("conv2", 28, 64, offset="(6,0,0)", to="(pool1-east)", height=64, depth=64, width=2, caption="Conv2"),
    #to_Pool("pool2", offset="(1,0,0)", height=64, depth=64, width=2, to="(conv2-east)", caption="Pool2"),
    
    #to_Conv("conv3", 28, 64, offset="(6,0,0)", to="(pool2-east)", height=64, depth=64, width=2, caption="Conv3"),
    #to_Pool("flat1", offset="(1,0,0)", height=64, depth=64, width=2, to="(conv3-east)", caption="Flat1"),

    #to_Pool3("dropout1", offset="(1,0,0)", height=64, depth=64, width=2, to="(flat1-east)", caption="Dropout1"),
    
    #to_Conv("dense1", 1, 512, offset="(5,0,0)", to="(batch3-east)", height=64, depth=64, width=2, caption="Dense1"),
    #to_Conv("dense2", 1, 10, offset="(1,0,0)", to="(dense1-east)", height=64, depth=64, width=2, caption="Dense2"),
    
    to_SoftMax("dense1", 512 ,"(0,0,0)", "(0,0,0)", depth=150, caption="Dense1"),
    to_SoftMax("dense2", 10 ,"(2,0,0)", "(dense1-east)", depth=25, caption="Dense2"),
    
    
    #to_Conv("conv2", 128, 64, offset="(1,0,0)", to="(pool1-east)", height=32, depth=32, width=2 ),
    #to_connection( "pool1", "conv2"),
    #to_Pool("pool2", offset="(0,0,0)", to="(conv2-east)", height=28, depth=28, width=1),
    #to_SoftMax("soft1", 10 ,"(3,0,0)", "(pool1-east)", caption="SOFT"  ),
    #to_connection("pool2", "soft1"),
    to_end()
    ]

def main():
    namefile = str(sys.argv[0]).split('.')[0]
    to_generate(arch, namefile + '.tex' )

if __name__ == '__main__':
    main()
