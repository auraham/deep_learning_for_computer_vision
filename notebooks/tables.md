| Input data                       | Shape                                                        |
| -------------------------------- | ------------------------------------------------------------ |
| Vector data                      | 2D tensors of shape `(samples, features)`.                   |
| Timeseries data or sequence data | 3D tensors of shape `(samples, timesteps, features)`.        |
| Images                           | 4D tensors of shape `(samples, height, width, channels)` or `(samples, channels, height, width).` |
| Video                            | 5D tensors of shape `(samples, frames, height, width, channels)` or `(samples, frames, channels, height, width)`. |



| Challenge             |                                                              |
| --------------------- | ------------------------------------------------------------ |
| Viewpoint variation   | It refers to the viewpoint of an object with respect to the camera. A single instance of an object can be oriented in many ways with respect to the camera. |
| Scale variation       | It refers to the size of an object. Visual classes often exhibit variation in their size (size in the real world, not only in terms of their extent in the image). For instance, a cup of coffee often has three sizes: small, medium, and large. In addition,we can take a picture of a cup of coffee close to the camera and far away from the camera. Both pictures contains the same cup, but they may look dramatically different. |
| Deformation           | Many objects of interest are not rigid bodies and can be deformed in extreme ways. |
| Occlusion             | The objects of interest can be occluded. Sometimes only a small portion of an object (as little as few pixels) could be visible. This happens when large parts of the object we want to classify are hidden from view in the image. |
| Illumination          | The same object can look dramatically different depending on the lighting conditions. For instance, if we took a photo of a cup of coffee using good lighting and another one with low lighting. |
| Background clutter    | The objects of interest may *blend* into their environment, making them hard to identify. For instance, the game of *Where is Waldo?*, where we need to find Waldo in a crowd. |
| Intra-class variation | The classes of interest can often be relatively broad, such as *chair*. There are many different types of these objects, each with their own appearance. |



A good image classification model must be invariant to the cross product of all these variations, while simultaneously retaining sensitivity to the inter-class variations.